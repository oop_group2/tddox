/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.waratchaya.xopro;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Melon
 */
public class TDDTest {

    public TDDTest() {
    }

    @Test
    public void testAdd_1_2is3() {
        assertEquals(3, Example.add(1, 2));
    }

    @Test
    public void testAdd_20_22is42() {
        assertEquals(42, Example.add(20, 22));
    }

    @Test
    public void testChop_p1_p_p2_p_is_draw() {
        assertEquals("draw", Example.chup('p', 'p'));
    }
    @Test
    public void testChop_p1_h_p2_h_is_draw() {
        assertEquals("draw", Example.chup('h', 'h'));
    }
    @Test
    public void testChop_p1_s_p2_s_is_draw() {
        assertEquals("draw", Example.chup('s', 's'));
    }
    @Test
    public void testChop_p1_s_p2_p_is_p1() {
        assertEquals("p1", Example.chup('s', 'p'));
    }
    @Test
    public void testChop_p1_h_p2_s_is_p1() {
        assertEquals("p1", Example.chup('h', 's'));
    }
    @Test
    public void testChop_p1_p_p2_h_is_p1() {
        assertEquals("p1", Example.chup('p', 'h'));
    }
    @Test
    public void testChop_p1_h_p2_p_is_p1() {
        assertEquals("p2", Example.chup('h', 'p'));
    }
    @Test
    public void testChop_p1_p_p2_s_is_p1() {
        assertEquals("p2", Example.chup('p', 's'));
    }
    @Test
    public void testChop_p1_s_p2_h_is_p1() {
        assertEquals("p2", Example.chup('s', 'h'));
    }
}
